from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Float, ARRAY

engine = create_engine('postgresql://postgres:P4ssw0rdT3mp@localhost:5432/exampledb',
                       pool_size=5,
                       max_overflow=10,
                       pool_recycle=-1,
                       pool_timeout=30)
Session = sessionmaker(bind=engine)
Base = declarative_base()


class Song(Base):
    __tablename__ = "exampledb"
    song_name = Column(String(50), nullable=False)
    artists_names = Column(ARRAY(String), nullable=False)
    # track = Column(String(50),nullable=False)
    played_timestamp = Column(String(50), primary_key=True)
    energy = Column(Float, nullable=False)
    tempo = Column(Float, nullable=False)
    speechiness = Column(Float, nullable=False)
    loudness = Column(Float, nullable=False)
    liveness = Column(Float, nullable=False)
    instrumentalness = Column(Float, nullable=False)
    acousticness = Column(Float, nullable=False)
    danceability = Column(Float, nullable=False)

    def __init__(self,
                 song_name,
                 artists_names,
                 played_timestamp,
                 energy,
                 tempo,
                 speechiness,
                 loudness,
                 liveness,
                 instrumentalness,
                 acousticness,
                 danceability):
        self.song_name = song_name
        self.artists_names = artists_names
        self.played_timestamp = played_timestamp
        self.energy = energy
        self.tempo = tempo
        self.speechiness = speechiness
        self.loudness = loudness
        self.liveness = liveness
        self.instrumentalness = instrumentalness
        self.acousticness = acousticness
        self.danceability = danceability

    def __repr__(self):
        return f'Song({self.song_name},' \
               f' {self.artists_names},' \
               f'{self.played_timestamp},' \
               f'{self.energy},' \
               f'{self.tempo},' \
               f'{self.speechiness},' \
               f'{self.loudness},' \
               f'{self.liveness},' \
               f'{self.instrumentalness},' \
               f'{self.acousticness},' \
               f'{self.danceability})'

    def __str__(self):
        return self.song_name


if __name__ == '__main__':
    # Base.metadata.create_all(engine)
    db_session = Session()
    db_session.add(Song('PruebaA',
                        ['a', 'b', 'c'],
                        'la hora',
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.2,
                        0.3,
                        0.4,
                        0.5))
    db_session.commit()
    db_session.close()
